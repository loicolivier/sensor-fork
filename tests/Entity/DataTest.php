<?php

namespace App\Tests\Entity;

use App\Entity\Data;
use PHPUnit\Framework\TestCase;
use TypeError;

class DataTest extends TestCase
{
    public function testTempatureNull(): void
    {
        $data = new Data();

        $this->assertNull($data->getTemperature());
    }

    public function testTemperatureIsNotNullAfterUpdate(): void
    {
        $data = new Data();
        $data->setTemperature(23.5);

        $this->assertNotNull($data->getTemperature());
    }

    public function testTemperatureEqualsAfterUpdate(): void
    {
        $data = new Data();
        $data->setTemperature(23.5);

        $this->assertEquals(23.5, $data->getTemperature());
    }

    public function testTemperatureTypeError(): void
    {
        $this->expectException(TypeError::class);

        $data = new Data();
        $data->setTemperature('sdvxwfv');
    }
}
