<?php

namespace App\Command;

use App\Manager\SensorManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;

class GetDataCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var SensorManager
     */
    private $sensorManager;

    protected static $defaultName = 'app:data:update';

    public function __construct(SensorManager $sensorManager)
    {
        parent::__construct();
        $this->sensorManager = $sensorManager;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function configure(): void
    {
        $this->setDescription('Retrieve data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->sensorManager->updateSensors();

        $this->io->success('Sensors were successfully updated !');

        return 0;
    }
}
