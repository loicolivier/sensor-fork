<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 16/08/21
 * Time: 14:43
 */

namespace App\Object;

use App\Entity\Sensor;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Doctrine\ORM\EntityManagerInterface;

class RequestParameter
{
    const FORMAT_ALL = 'all';
    const FORMAT_AVERAGE = 'average';
    const FORMAT_VALUE = 'value';
    const FORMAT_MINIMUM = 'minimum';
    const FORMAT_MAXIMUM = 'maximum';

    private $value;

    private $interval;

    private $request;

    private $begin;

    private $end;

    private $format;

    private $sensors;

    private $em;

    public function addDevice(string $device = null)
    {
        if(!$device) {
            return;
        }
        $sensor = $this->em->getRepository('App:Sensor')->findOneBydevEUI($device);
        if($sensor) {
            $this->addSensor($sensor);
        }
    }

    protected function addDevices(): void
    {
        /** @var mixed $result */
        $result = $this->request->query->get('device');
        if (is_array($result)) {
            /** @var array $result */
            foreach($result as $device) {
                $this->addDevice($device);
            }
        } else {
            $this->addDevice($result);
        }
    }


    function __construct(EntityManagerInterface $em, Request $request)
    {
        $this->begin = null;
        $this->end = null;
        $this->request = $request;
        $this->sensors = new ArrayCollection();
        $this->em = $em;

        // parameters
        $this->interval = $this->request->query->get('interval', 0);
        $this->format = $this->request->query->get('format', self::FORMAT_ALL);
        $this->value = $this->request->query->get('value', 'temperature');
        $begin = $this->request->query->get('begin');
        try {
            if($begin) {
                $this->begin = new \Datetime($begin);
            }
        } catch (\Exception $e) {

        }
        if(!$this->begin) {
            $this->begin = new \DateTime('now');
            $this->begin->sub(new \DateInterval('P7D'));
        }
        $end = $this->request->query->get('end');
        try {
            if($end) {
                $this->end = new \Datetime($end);
            }
        } catch (\Exception $e) {

        }
        $this->addDevices();
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getBegin(): ?\DateTimeInterface
    {
        return $this->begin;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function getInterval(): int
    {
        return $this->interval;
    }

    public function getSensors(): Collection
    {
        return $this->sensors;
    }

    public function addSensor(Sensor $sensor): self
    {
        if (!$this->sensors->contains($sensor)) {
            $this->sensors[] = $sensor;
        }

        return $this;
    }

    public function removeSensor(Sensor $sensor): self
    {
        $this->sensors->removeElement($sensor);

        return $this;
    }
}