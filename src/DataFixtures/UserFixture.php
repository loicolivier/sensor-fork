<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class UserFixture extends Fixture
{
    /** @var PasswordHasherInterface */
    private $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $manager->persist($this->createSampleUser());
        $manager->persist($this->createAdminUser());


        $manager->flush();
    }

    private function createAdminUser(): User
    {
        $user = new User();

        $user
            ->setEmail('admin@api-sensor.com')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($this->userPasswordHasher->hashPassword($user, 'azerty'))
        ;

        return $user;
    }

    private function createSampleUser(): User
    {
        $user = new User();

        $user
            ->setEmail('sample@api-sensor.com')
            ->setPassword($this->userPasswordHasher->hashPassword($user, 'azerty'))
        ;

        return $user;
    }
}
