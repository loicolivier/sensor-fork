
const ADMIN_EMAIL = "admin@api-sensor.com";
const ADMIN_PASSWORD = "azerty";

beforeEach(() => {
    cy.visit('/login');

    cy.intercept('GET', '/admin/sensor').as('sensor_index');
})

it('allows logging in with an admin user', () => {

    cy.get('input[name=email]').type(ADMIN_EMAIL);
    cy.get('input[name=password]').type(ADMIN_PASSWORD);
    cy.get('form').submit();
  
    cy.contains('Create new').click()

    cy.get('#sensor_name').type('Capteur1');
    cy.get('#sensor_devEUI').type('666');
    cy.get('#sensor_type').type('sensor de la mort');
    cy.get('#sensor_latitude').type('45.9198');
    cy.get('#sensor_longitude').type('5.5647');

    cy.contains('Save').click()

    cy.contains('Capteur1').contains('edit').click()


   

});


