// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add('login', (email, password) => {

    cy.request({url: '/login', log: false})
        .its('body', {log: false})
        .then((body) => {
            const csrf = Cypress.$(body).find('input[name="_csrf_token"]').val();

            cy.request({
                method: 'POST',
                url: '/login',
                followRedirect: false,
                form: true,
                body: {
                    email: email,
                    password: password,
                    _csrf_token: csrf,
                },
                log: false,
            })
            .then((resp) => {
                expect(resp.redirectedToUrl).to.not.have.string('/login');
            });
        });
})
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
