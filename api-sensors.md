
# Url
/api/v1/datas/
{
  [
  {
  "object": <string>,
  "object-id": <string>,
  "object-type": <string>,
  <value>: <float>,
  "average": <float>,
  "min": <float>,
  "max": <float>
  "timestamp": <time>,
  "minimum": <time>,
  "maximum": <time>
  },
  ...
  ]
}


/api/v1/datas/devices/<device-id>


# Parameters
Valids on both api :

* begin <datetime> (default : begin = now - 7 days).
* end = <datetime> default now
* interval = <integer> (hours| default:24 | between 0 and 24 (more than 24 consider as 24))
* format=<string> all (default)|value|average|min|max
* device=<string> dev-id (default:none), device[] to set various devices.
* value=<string> (temperature (default)|humidity|pressure)
